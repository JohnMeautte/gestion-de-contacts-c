
NOM = gestionnaire_contacts
CC = g++
CFLAGS := -Wall 
LFLAGS := -l sqlite3 
SRC = $(wildcard src/*/*.cpp)
SRC += src/main.cpp
OBJ = $(SRC:.cpp=.o)


all: $(NOM)

%.o: %.cpp
	$(CC) $(CFLAGS) -o $@ -c $< -std=c++11 

$(NOM): $(OBJ)
	$(CC) $(CFLAGS) -o $(NOM) $^ $(LFLAGS)



re: clean clean_exec all




clean:
	rm -r $(OBJ) 

clean_exec:
	rm $(NOM)
