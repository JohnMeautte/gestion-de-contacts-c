#include <iostream>
#include "../headers/main_functions.hpp"

using namespace std;

// ajouter un contact 
void ajouter_contact(Annuaire& annu, char* path){
    string propri, nom, prenom, sexe, rue, complement, code_postal, ville, nom_entreprise, mail, date_naissance;
    int id, numero_rue;
    cout << "Contact Privé ou Pro? (pro/prive) ";
    cin >> propri;

    // Si c'est un conctact pro
    if(propri == "pro"){
        cout << endl << "Id: "; cin >> id;
        cout << endl << "NOM: "; getline(cin >> ws, nom);
        cout << endl << "Prenom: "; getline(cin, prenom);
        cout << endl << "Sexe (M/F): "; getline(cin, sexe);
        cout << endl << "Entreprise: "; getline(cin, nom_entreprise);
        cout << endl << "Numero adresse entreprise: "; cin >> numero_rue;
        cout << endl << "Rue entreprise: "; getline(cin >> ws, rue);
        cout << endl << "Complement adresse: "; getline(cin, complement);
        cout << endl << "Code postal: "; getline(cin, code_postal);
        cout << endl << "Ville: "; getline(cin, ville);
        cout << endl << "Mail: "; getline(cin, mail);

        // on crée et ajoute le contact à l'annuaire et la db
        ContactPro* cont = new ContactPro(id, nom, prenom, sexe, nom_entreprise,
            new Adresse(numero_rue, rue, complement, code_postal, ville),
            mail);
        annu.ajouter(cont);
        insert(path, cont);
    }
    // Si c'est un contact privé
    else if(propri == "prive"){
        cout << endl << "Id: "; cin >> id;
        cout << endl << "NOM: "; getline(cin >> ws, nom);
        cout << endl << "Prenom: "; getline(cin, prenom);
        cout << endl << "Sexe (M/F): "; getline(cin, sexe);
        cout << endl << "Numero adresse: "; cin >> numero_rue;
        cout << endl << "Rue: "; getline(cin >> ws, rue);
        cout << endl << "Complement adresse: "; getline(cin, complement);
        cout << endl << "Code postal: "; getline(cin, code_postal);
        cout << endl << "Ville: "; getline(cin, ville);
        cout << endl << "Date de naissance (aaaa-mm-jj): "; getline(cin, date_naissance);

        // on crée et ajoute le contact à l'annuaire et la db
        ContactPrive* cont = new ContactPrive(id, nom, prenom, sexe,
            new Adresse(numero_rue, rue, complement, code_postal, ville),
            date_naissance);
        annu.ajouter(cont);
        insert(path, cont);
    }
    // Autre: mauvais choix
    else
        cout << "Mauvais choix" << endl;


}

// retirer un contact selon l'id
void retirer_contact(Annuaire& annu, char* path){
    int id;
    cout << "Donner l'id du contact à retirer: "; cin >> id;
    annu.retirer(id);
    delete_contact(path, id);
}

// afficher les contacts vivant dans une ville indiquée
void affichage_contact_via_ville(Annuaire& annu){
    string ville;
    cout << "Quelle est le nom de la ville: ";
    getline(cin >> ws, ville);
    annu.afficher_par_ville(ville);
}

// affihcer les contacts ayant le nom indiqué
void affichage_contact_via_nom(Annuaire& annu){
    string nom;
    cout << "Quel est le nom du contact: ";
    getline(cin >> ws, nom);
    annu.afficher_par_nom(nom);
}

// affiche l'interface principale du programme
void affiche_interface(Annuaire& annu, char* path){
    string choix;

    // lit la db pour mettre dans l'annuaire
    open_and_read_database(path, annu);

    // interface
    while(1){
        cout << "\n\n===== GESTIONNAIRE DE CONTACTS =====" << endl;
        cout << "Faire un choix:" << endl;
        cout << "\t1- Afficher les contacts" << endl;
        cout << "\t2- Ajouter un contact" << endl;
        cout << "\t3- Supprimer un contact via l'id" << endl;
        cout << "\t4- Rechercher un contact via sa ville" << endl;
        cout << "\t5- Rechercher un contact via son nom" << endl;
        cout << "\n\t0- Quitter l'application" << endl;
        cout << "===== CHOIX: ";
        cin >> choix;

        // quitter
        if (choix == "0"){
            cout << "Au revoir" << endl;
            break;
        }

        fonction_choix(stoi(choix), annu, path);
    }
}

// choisit l'action selon le choix
void fonction_choix(int choix, Annuaire& annu, char* path){
    switch(choix){
        case 1: annu.afficher(); break;
        case 2: ajouter_contact(annu, path); break;
        case 3: retirer_contact(annu, path); break;
        case 4: affichage_contact_via_ville(annu); break;
        case 5: affichage_contact_via_nom(annu); break;
        default: cout << "Mauvais choix" << endl; break;
    }
}
