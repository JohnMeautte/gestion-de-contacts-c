#include <iostream>

#include "../headers/sqlite_functions.hpp"

using namespace std;


// lit et met la database dans l'annuaire annu
void open_and_read_database(char* path, Annuaire &annu){
    int rc, id, numero;
    sqlite3_stmt *stmt;
    sqlite3 *db=nullptr;

    char *entreprise;
    char *date;
    char *complement;
    char *mail;
    string nom, prenom, sexe, ad, code_postal, ville, rue;

    // Ouverture
    rc = sqlite3_open(path, &db);
    if(rc)
        cout << "- Database non ouverte: " << sqlite3_errmsg(db) << endl;
    else
        cout << "- Database ouverte" << endl;


    sqlite3_prepare_v2(db,
                       "select * from contacts",
                       -1,
                       &stmt, NULL);

    // on parcourt la db et on met dans des variables
    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW){
        id = sqlite3_column_int(stmt, 0);
        nom = string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 1)));
        prenom = string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 2)));
        sexe = string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 3)));
        entreprise = (char *)sqlite3_column_text(stmt, 4);
        ad = string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 5)));
        complement = (char*)sqlite3_column_text(stmt, 6);
        code_postal = string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 7)));
        ville = string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 8)));
        mail = (char*)sqlite3_column_text(stmt, 9);
        date = (char*)sqlite3_column_text(stmt, 10);


        split_address_db(ad, numero, rue);

        // On vérifie si ce n'est pas NULL
        // si c'est pas NULL, c'est un ContactPro
        if(entreprise){
            // Si complement = NULL, on modifie pour éviter les erreurs
            if(!complement) 
                complement = (char*)("");
            annu.ajouter(new ContactPro(id, 
                nom, 
                prenom, 
                sexe, 
                entreprise, 
                new Adresse(numero, rue, complement, code_postal, ville), 
                mail));

        }
        else{
            // Si complement = NULL, on modifie pour éviter les erreurs
            if(!complement)
                complement = (char*)("");
            annu.ajouter(new ContactPrive(id,
                nom,
                prenom,
                sexe,
                new Adresse(numero, rue, complement, code_postal, ville), 
                date));
        }

    }

    sqlite3_finalize(stmt);
    db_close(db);
}

// split l'adresse pour récupérer le numéro et la rue dans la db
void split_address_db(string &adresse, int &numero, string &rue){
    string lettre;

    // on parcourt toute l'adresse
    for (int i = 0; i < adresse.length(); ++i){
        // moment de séparer le numéro et la rue
        if (adresse[i] == ','){
            numero = stoi(lettre);
            lettre = "";
            i+=2; // on incrémente pour éviter le ", "
        }
        lettre += adresse[i];
    }
    rue = lettre;

}

// ferme la db
void db_close(sqlite3* db){
    int rc;
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) 
        printf("ERREUR Close : %s\n", sqlite3_errmsg(db));
    else
        cout << "- Database fermée" << endl;
}

// insert des valeurs d'un contact pro dans la db
void insert(char* path, ContactPro* cont){
    int rc;
    char *errmsg = 0;
    string sql = "INSERT INTO CONTACTS VALUES(";

    sqlite3 *db=nullptr;
    rc = sqlite3_open(path, &db);
    cout << "- Database ouverte pour insertion" << endl;


    // préparation de la commande sql
    sql += to_string(cont->get_id()); sql+=", ";
    sql += "'"; sql += cont->get_nom(); sql +="', ";
    sql += "'"; sql += cont->get_prenom(); sql +="', ";
    sql += "'"; sql += cont->get_sexe(); sql +="', ";
    sql += "'"; sql += cont->get_nom_entreprise(); sql +="', ";
    sql += "'"; sql += to_string(cont->adresse->get_numero()); sql +=", ";
    sql += cont->adresse->get_rue(); sql +="', ";
    sql += "'"; sql += cont->adresse->get_complement(); sql +="', ";
    sql += "'"; sql += cont->adresse->get_code_postal(); sql +="', ";
    sql += "'"; sql += cont->adresse->get_ville(); sql +="', ";
    sql += "'"; sql += cont->get_mail(); sql +="', ";
    sql += "NULL);";

    cout << sql << endl;
    // on passe par un pointeur 
    char *psql = const_cast<char*>(sql.c_str());
    rc = sqlite3_exec(db, psql, NULL, NULL, &errmsg);

    if (rc != SQLITE_OK){
        cout << "Erreur dans insertion données" << endl;
        printf("erreur: %s\n", errmsg);
        sqlite3_free(errmsg);
    }

    db_close(db);
    
    

}

// insert des valeurs d'un contact privé dans la db
void insert(char* path, ContactPrive* cont){
    int rc;
    char *errmsg = 0;
    string sql = "INSERT INTO CONTACTS VALUES(";

    sqlite3 *db=nullptr;
    rc = sqlite3_open(path, &db);
    cout << "- Database ouverte pour insertion" << endl;

    // préparation commande sql
    sql += to_string(cont->get_id()); sql+=", ";
    sql += "'"; sql += cont->get_nom(); sql +="', ";
    sql += "'"; sql += cont->get_prenom(); sql +="', ";
    sql += "'"; sql += cont->get_sexe(); sql +="', ";
    sql += "NULL, ";
    sql += "'"; sql += to_string(cont->adresse->get_numero()); sql +=", ";
    sql += cont->adresse->get_rue(); sql +="', ";
    sql += "'"; sql += cont->adresse->get_complement(); sql +="', ";
    sql += "'"; sql += cont->adresse->get_code_postal(); sql +="', ";
    sql += "'"; sql += cont->adresse->get_ville(); sql +="', ";
    sql += "NULL, ";
    sql += "'"; sql += cont->get_date_naissance(); sql +="');";

    cout << sql << endl;
    // on passe par un pointeur
    char *psql = const_cast<char*>(sql.c_str());
    rc = sqlite3_exec(db, psql, 0, 0, &errmsg);

    if (rc != SQLITE_OK){
        cout << "Erreur dans insertion données" << endl;
        printf("erreur: %s\n", errmsg);
        sqlite3_free(errmsg);
    }

    db_close(db);
    
}

// supprimer un contact selon son id
void delete_contact(char* path, int id){
    int rc;
    char *errmsg = 0;
    string sql = "DELETE FROM CONTACTS WHERE IdContact = " + to_string(id);
    char *psql = const_cast<char*>(sql.c_str());

    sqlite3 *db=nullptr;
    rc = sqlite3_open(path, &db);
    cout << "- Database ouverte pour suppression" << endl;

    rc = sqlite3_exec(db, psql, 0, 0, &errmsg);

    if (rc != SQLITE_OK){
        cout << "Erreur dans suppression données" << endl;
        printf("erreur: %s\n", errmsg);
        sqlite3_free(errmsg);
    }

    db_close(db);


}

















