#include <iostream>

#include "classes/ContactPrive.hpp"
#include "classes/ContactPro.hpp"
#include "classes/Annuaire.hpp"
#include "headers/main_functions.hpp"

using namespace std;


int main(int argc, char const *argv[])
{
    
    // création annuaire
    Annuaire annu("annuaire de john");

    // chemin vers la db
    char *path = const_cast<char*>(argv[1]);

    // fonction principale qui permet l'interface du programme
    affiche_interface(annu, path);



    return 0;
}



