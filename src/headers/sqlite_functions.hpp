#ifndef SQLITE_FUNCTIONS_HEADER
#define SQLITE_FUNCTIONS_HEADER

#include <sqlite3.h>
#include "../classes/Annuaire.hpp"
#include "../classes/ContactPrive.hpp"
#include "../classes/ContactPro.hpp"

void open_and_read_database(char*, Annuaire&);
void split_address_db(std::string&, int&, std::string&);
void db_close(sqlite3*);
void insert(char*, ContactPrive*);
void insert(char*, ContactPro*);
void delete_contact(char*, int);


#endif
