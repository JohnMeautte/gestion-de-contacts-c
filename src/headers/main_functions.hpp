#ifndef MAIN_FUNC_HEADER
#define MAIN_FUNC_HEADER

#include "sqlite_functions.hpp"

void ajouter_contact(Annuaire&, char*);

void retirer_contact(Annuaire&, int ,char*);

void affiche_interface(Annuaire&, char*);
void fonction_choix(int choix, Annuaire& annu, char* path);
void affichage_contact_via_ville(Annuaire&);

#endif
