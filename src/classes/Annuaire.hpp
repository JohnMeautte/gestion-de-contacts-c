#ifndef ANNUAIRE_HEADER
#define ANNUAIRE_HEADER

#include <map>
#include "Contact.hpp"

class Annuaire
{
public:
    Annuaire(std::string);
    ~Annuaire();

    void set_nom_annuaire(std::string);
    std::string get_nom(){return nom_annuaire;}

    void afficher();
    void ajouter(Contact*);
    void retirer(int);

    void afficher_par_ville(std::string&);
    void afficher_par_nom(std::string& nom);

private:
    std::string nom_annuaire;
    std::map<int, Contact*> contacts;
    
};


#endif
