#ifndef ADRESSE_HEADER
#define ADRESSE_HEADER

class Adresse
{
public:
    Adresse(int=0, std::string="rue", std::string="complement", int=00000, std::string="ville");
    Adresse(int=0, std::string="rue", std::string="complement", std::string="00000", std::string="ville");
    ~Adresse();


    void set_numero(int);
    void set_rue(std::string);
    void set_complement(std::string);
    void set_code_postal(std::string);
    void set_ville(std::string);

    int get_numero(){return this->numero;}
    std::string get_rue(){return this->rue;}
    std::string get_complement(){return this->complement;}
    std::string get_code_postal(){return this->code_postal;}
    std::string get_ville(){return this->ville;}

    void infos();

private:
    int numero;
    std::string rue;
    std::string complement;
    std::string code_postal;
    std::string ville;
    
};




#endif
