#include <iostream>
#include "ContactPro.hpp"

using namespace std;


ContactPro::ContactPro(int id, string nom, string prenom, string sexe, string nom_entreprise, Adresse* adresse, string mail):Contact(id, nom, prenom, sexe, adresse){
    this->set_nom_entreprise(nom_entreprise);
    this->set_mail(mail);

}

ContactPro::~ContactPro(){};

void ContactPro::set_nom_entreprise(string nom){
    // Si longueur du nom <= 50, on assigne
    try{
        if (nom.length() <= 50){
            transform(nom.begin(), nom.end(), nom.begin(), ::toupper);
            this->nom_entreprise = nom;
        }
        else
            throw(nom.length());
    } // sinon erreur
    catch(unsigned long length){
        cout << "  ERREUR: longueur nom entreprise supérieure à 50 lettres" << endl;
        cout << "  longueur nom entreprise: " << length << endl << endl;
    }
}       


void ContactPro::set_mail(string mail){
    // Si il y a bien un @ dans le mail, on assigne
    try{
        if(mail.find('@') != string::npos)
            this->mail = mail;
        else
            throw(mail);
    } // sinon erreur
    catch(string m){
        cout << "  ERREUR: mail non conforme, ne contient pas de @" << endl;
        cout << "  mail indiqué: " << m << endl << endl;
    }
}

// affiche les infos du contact pro
void ContactPro::infos(){
    Contact::infos();
    cout << "\tentreprise " << this->get_nom_entreprise() << endl;
    cout << "\n\tMAIL: " << this->get_mail() << endl;
}
