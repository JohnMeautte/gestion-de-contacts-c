#ifndef CONTACT_HEADER
#define CONTACT_HEADER

#include "Adresse.hpp"

class Contact
{
public:
    Contact(int, std::string, std::string, std::string, Adresse*);
    virtual ~Contact();

    int get_id(){return id;}
    std::string get_nom(){return nom;}
    std::string get_prenom(){return prenom;}
    std::string get_sexe(){return sexe;}

    void set_id(int);
    void set_nom(std::string);
    void set_prenom(std::string);
    void set_sexe(std::string);
    void set_adresse(Adresse*);

    virtual void infos();

    Adresse* adresse;

private:
    int id;
    std::string nom;
    std::string prenom;
    std::string sexe;

};



#endif
