#include <iostream>
#include <iomanip>

#include "Annuaire.hpp"
#include "ContactPrive.hpp"
#include "ContactPro.hpp"

#include "../headers/sqlite_functions.hpp"

using namespace std;

Annuaire::Annuaire(string nom){
    this->set_nom_annuaire(nom_annuaire);
}

Annuaire::~Annuaire(){

    for (auto itr = this->contacts.begin(); itr != this->contacts.end(); ++itr) {
        delete itr->second;
    }
    contacts.clear();
}

void Annuaire::set_nom_annuaire(string nom){
    this->nom_annuaire = nom;
}

// affiche les infos des contacts
void Annuaire::afficher(){

    for (auto itr = this->contacts.begin(); itr != this->contacts.end(); ++itr) {
        cout << endl ;

        ContactPro *Cpro = dynamic_cast<ContactPro*>(itr->second);
        ContactPrive *Cprive = dynamic_cast<ContactPrive*>(itr->second);

        // Si c'est un contact pro
        if(Cpro){
            cout << "PRO: " << setw(5) << setfill('0') << itr->second->get_id()<< endl;
            itr->second->infos();
        }// Si c'est un contact prive
        else if(Cprive){
            cout << "PRIVE: " << setw(5) << setfill('0') << itr->second->get_id()<< endl;
            itr->second->infos();
        }// autre
        else{
            cout << "CONTACT NORMAL: " << setw(5) << setfill('0') << itr->second->get_id()<< endl;
            itr->second->infos();
        }
    }
}

// affiche les contacts de cette ville
void Annuaire::afficher_par_ville(string& ville){
    // on met en majuscule pour éviter les erreurs
    transform(ville.begin(), ville.end(), ville.begin(), ::toupper);

    for (auto itr = this->contacts.begin(); itr != this->contacts.end(); ++itr) {

        ContactPro *Cpro = dynamic_cast<ContactPro*>(itr->second);
        ContactPrive *Cprive = dynamic_cast<ContactPrive*>(itr->second);

        // Si le contact appartient à la ville
        if(itr->second->adresse->get_ville() == ville){
            cout << endl ;
            // Si contact pro
            if(Cpro){
                cout << "PRO: " << setw(5) << setfill('0') << itr->second->get_id()<< endl;
                itr->second->infos();
            }// si contact prive
            else if(Cprive){
                cout << "PRIVE: " << setw(5) << setfill('0') << itr->second->get_id()<< endl;
                itr->second->infos();
            }// autre
            else{
                cout << "CONTACT NORMAL: " << setw(5) << setfill('0') << itr->second->get_id()<< endl;
                itr->second->infos();
            }
        }
    }
}

// affiche les contacts avec ce nom
void Annuaire::afficher_par_nom(string& nom){
    // on met en majuscule pour éviter les erreurs
    transform(nom.begin(), nom.end(), nom.begin(), ::toupper);

    for (auto itr = this->contacts.begin(); itr != this->contacts.end(); ++itr) {

        ContactPro *Cpro = dynamic_cast<ContactPro*>(itr->second);
        ContactPrive *Cprive = dynamic_cast<ContactPrive*>(itr->second);

        // Si le contact a le même nom qu'indiqué
        if(itr->second->get_nom() == nom){
            cout << endl ;
            // si contact pro
            if(Cpro){
                cout << "PRO: " << setw(5) << setfill('0') << itr->second->get_id()<< endl;
                itr->second->infos();
            }// si contact prive
            else if(Cprive){
                cout << "PRIVE: " << setw(5) << setfill('0') << itr->second->get_id()<< endl;
                itr->second->infos();
            }// autre
            else{
                cout << "CONTACT NORMAL: " << setw(5) << setfill('0') << itr->second->get_id()<< endl;
                itr->second->infos();
            }
        }
    }
}

// ajouter un contact à l'annuaire
void Annuaire::ajouter(Contact* contact){
    // si id n'est pas déjà utilisé
    try{
        int id = contact->get_id();
        auto it = this->contacts.find(id);
        if (it == contacts.end()){
            this->contacts.insert({id, contact});
            
            cout << contact->get_nom() << " " << contact->get_prenom() << " ajouté" << endl;

        }
        else
            throw(id);
    }
    catch(int i){
        cout << "  ERREUR: id indiqué déjà présent dans l'annuaire" << endl;
        cout << "  id indiqué: " << i << endl << endl; 
    }
}


// retire un contact selon son id
void Annuaire::retirer(int id){
    // on regarde si l'id existe
    try{
        auto it = this->contacts.find(id);

        if (it != contacts.end()){
            delete this->contacts[id];
            this->contacts.erase(contacts.find(id));
            cout << "Contact retiré" << endl;
        }
        else
            throw(id);    
    } // sinon erreur
    catch(int i){
        cout << "  ERREUR: id indiqué ne correspond à aucun dans l'annuaire" << endl;
        cout << "  id indiqué: " << i << endl << endl; 
    }
}
