#ifndef CONTACT_PRIVE_HEADER
#define CONTACT_PRIVE_HEADER

#include "Contact.hpp"
#include "Adresse.hpp"


class ContactPrive: public Contact
{
public:
    // ContactPrive(int=0, std::string="NOM", std::string="Prenom", std::string="H", Adresse*=nullptr, std::string="00/00/0000");
    ContactPrive(int, std::string, std::string, std::string, Adresse*, std::string);
    ~ContactPrive();

    void set_adresse(Adresse*);
    void set_date_naissance(std::string);

    std::string get_date_naissance(){return date_naissance;}

    void infos();

    

    Adresse* adresse_postale;
private:
    std::string date_naissance;
};


#endif
