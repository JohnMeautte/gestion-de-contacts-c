#ifndef CONTACT_PRO_HEADER
#define CONTACT_PRO_HEADER

#include "Contact.hpp"


class ContactPro: public Contact
{
public:
    // ContactPrive(int=0, std::string="NOM", std::string="Prenom", std::string="H", Adresse*=new Adresse(), std::string="00/00/0000");
    ContactPro(int, std::string, std::string, std::string, std::string, Adresse*, std::string);
    ~ContactPro();

    void set_nom_entreprise(std::string);
    void set_mail(std::string);
    

    std::string get_nom_entreprise(){return nom_entreprise;}
    std::string get_mail(){return mail;}

    void infos();

    

private:
    std::string nom_entreprise;
    std::string mail;
};


#endif
