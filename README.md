# Gestion de contacts C++


## Introduction

Ce projet est un gestionnaire de contacts, personnels (privés) ou professionnels (pro). Il permet d'associer une base données .db qu'il faut préciser durant l'exécution du programme. Tous les contacts dans cette base de données seront alors récupérés dans le programme et il sera possible d'en supprimer, d'en rajouter ou de faire des recherches parmi eux. (ATTENTION, si la base de données ne contient pas la table précise utilisée pendant le développement, elle ne sera pas créée et le programme ne fonctionnera pas, cf partie structure table sqlite)
Ce projet est entièrement écrit en C++.

## Structure des fichiers

```bash

├── Makefile
├── README.md
├── gestionnaire_contacts
└── src
    ├── classes
    │   ├── Adresse.cpp
    │   ├── Adresse.hpp
    │   ├── Annuaire.cpp
    │   ├── Annuaire.hpp
    │   ├── Contact.cpp
    │   ├── Contact.hpp
    │   ├── ContactPrive.cpp
    │   ├── ContactPrive.hpp
    │   ├── ContactPro.cpp
    │   └── ContactPro.hpp
    ├── func
    │   ├── main_functions.cpp
    │   └── sqlite_functions.cpp
    ├── headers
    │   ├── main_functions.hpp
    │   └── sqlite_functions.hpp
    └── main.cpp
```

 - src/classes contient toutes les classes de l'annuaire de contacts
 - func contient les fonctions principales de fonctionnement du programme
 - headers contient les headers des fonctions dans func
 - main.cpp contient la fonction main
 - gestionnaire_contacts est le nom du programme à exécuter



## Installation

Pour installer le programme, il faut se mettre dans le dossier avec le Makefile et écrire les commandes suivantes. (ATTENTION, il faut bien préciser le chemin vers la base de données en argument)

```bash
make
./gestionnaire_contacts $CHEMIN_VERS_DATABASE
```

Pour relancer la compilation:
```bash
make re
```

Pour supprimer les fichiers .o:
```bash
make clean
```

Pour supprimer le fichier gestionnaire_contacts:
```bash
make clean_exec
```

## Structure table sqlite

```bash
TABLE CONTACTS
(
   	IdContact	integer primary key autoincrement not null,		
	Nom		varchar(30) NOT NULL,	
	Prenom		varchar(30) ,		
	Sexe		char(1) ,		
	Entreprise	varchar(50) ,			
	rue		varchar(250) ,
	Complement	varchar(250) ,	
	cp	        char(5),	
	Ville		varchar(100) ,		
	mail		varchar(250) ,		
	dtNaissance	date	
)

```

